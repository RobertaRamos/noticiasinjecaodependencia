package br.com.alura.technews.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import br.com.alura.technews.R
import br.com.alura.technews.model.Noticia
import br.com.alura.technews.ui.activity.extensions.transacaoFragment
import br.com.alura.technews.ui.fragment.FormularioNoticiaFragment
import br.com.alura.technews.ui.fragment.ListaNoticiasFragment
import br.com.alura.technews.ui.fragment.VisualizaNoticiaFragment
import kotlinx.android.synthetic.main.activity_noticias.*

private const val TAG_FRAGMENT_VISUALIZA_NOTICIA = "visualizaNoticia"

private const val TAG_FRAGMENT_FORMULARIO_NOTICIA = "formularioNoticia"


class NoticiasActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_noticias)

        configuraFragmentPeloEstado(savedInstanceState)
    }

    private fun configuraFragmentPeloEstado(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            abreListaNoticias()
        } else {
            tentaAbrirFragmentVisualizaNoticia()
            tentaAbrirFragmentFormularioNoticia()
        }
    }

    private fun tentaAbrirFragmentVisualizaNoticia() {
        supportFragmentManager.findFragmentByTag(TAG_FRAGMENT_VISUALIZA_NOTICIA)?.let { fragment ->

                val argumentos = fragment.arguments
                val novoFragment = VisualizaNoticiaFragment()
                novoFragment.arguments = argumentos

                removeFragment(fragment)

                transacaoFragment {
                    val container = configuraContainerFragmentVisualizaNoticia()
                    replace(container, novoFragment, TAG_FRAGMENT_VISUALIZA_NOTICIA)
                }
            }
    }

    private fun tentaAbrirFragmentFormularioNoticia(){
        supportFragmentManager.findFragmentByTag(TAG_FRAGMENT_FORMULARIO_NOTICIA)?.let {
            val argumentos = it.arguments
            val formularioNoticiaFragment = FormularioNoticiaFragment()
            formularioNoticiaFragment.arguments = argumentos

            removeFragment(it)

            transacaoFragment {
                addToBackStack(null)
                replace(R.id.activity_noticias_contanier_primario, formularioNoticiaFragment, TAG_FRAGMENT_FORMULARIO_NOTICIA)
            }
        }
    }

    private fun FragmentTransaction.configuraContainerFragmentVisualizaNoticia(): Int {
        if (activity_noticias_contanier_secundario != null) {
           return R.id.activity_noticias_contanier_secundario
        }
            addToBackStack(null)
            return R.id.activity_noticias_contanier_primario

    }

    private fun abreListaNoticias() {
        transacaoFragment {
            replace(R.id.activity_noticias_contanier_primario, ListaNoticiasFragment())
        }
    }

    private fun abreFormularioEdicao(noticia: Noticia) {
        removeFragmentVisualizaNoticia()
        val formularioNoticiaFragment = FormularioNoticiaFragment()
        val dados = Bundle()
        dados.putLong(NOTICIA_ID_CHAVE, noticia.id)
        formularioNoticiaFragment.arguments = dados
        transacaoFragment {
            addToBackStack(null)
            replace(R.id.activity_noticias_contanier_primario, formularioNoticiaFragment, TAG_FRAGMENT_FORMULARIO_NOTICIA)
        }

    }

    override fun onAttachFragment(fragment: Fragment?) {
        super.onAttachFragment(fragment)
        when(fragment){
            is ListaNoticiasFragment ->{
                configuraListaNoticias(fragment)
            }
            is VisualizaNoticiaFragment->{
                configuraVisualizaNoticias(fragment)
            }
            is FormularioNoticiaFragment -> {
                configuraFormularioNoticia(fragment)
            }
        }
    }

    private fun configuraFormularioNoticia(fragment: FormularioNoticiaFragment) {
        fragment.quandoFinalizaTela = { removeFragment(fragment) }
    }

    private fun configuraVisualizaNoticias(fragment: VisualizaNoticiaFragment) {
        fragment.quandoFinalizaTela = this::removeFragmentVisualizaNoticia
        fragment.quandoSelecionaMenuEdicao = this::abreFormularioEdicao
    }

    private fun removeFragmentVisualizaNoticia() {
        supportFragmentManager.findFragmentByTag(TAG_FRAGMENT_VISUALIZA_NOTICIA)?.let { fragment ->
            removeFragment(fragment)
        }
    }

    private fun removeFragment(fragment: Fragment) {
        transacaoFragment {
            remove(fragment)
        }
        supportFragmentManager.popBackStack()
    }

    private fun configuraListaNoticias(fragment: ListaNoticiasFragment) {
        fragment.quandoNoticiaSelecionada = this::abreVisualizadorNoticia
        fragment.quandoFabSalvaNoticiaClicado = this::abreFormularioModoCriacao

    }

    private fun abreFormularioModoCriacao() {
        transacaoFragment {
            addToBackStack(null)
            replace(R.id.activity_noticias_contanier_primario, FormularioNoticiaFragment(), TAG_FRAGMENT_FORMULARIO_NOTICIA)
        }
    }

    private fun abreVisualizadorNoticia(noticia: Noticia) {
        val fragment = VisualizaNoticiaFragment()
        val dados = Bundle()
        dados.putLong(NOTICIA_ID_CHAVE, noticia.id)
        fragment.arguments = dados
        transacaoFragment {
            val container = configuraContainerFragmentVisualizaNoticia()
            replace(container, fragment, TAG_FRAGMENT_VISUALIZA_NOTICIA)
        }
    }
}
